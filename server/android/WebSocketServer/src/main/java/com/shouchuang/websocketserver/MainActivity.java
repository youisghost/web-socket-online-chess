package com.shouchuang.websocketserver;

import android.app.Activity;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {
	private MainActivity main;
	private static Intent serviceIntent;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		main = this;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		((Button)findViewById(R.id.StartBtn)).setEnabled(serviceIntent == null);
		((Button)findViewById(R.id.StopBtn)).setEnabled(serviceIntent != null);
		((Button)findViewById(R.id.StartBtn)).setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				serviceIntent = new Intent(main, ServerService.class);
				startForegroundService(serviceIntent);
				((Button)findViewById(R.id.StartBtn)).setEnabled(false);
				((Button)findViewById(R.id.StopBtn)).setEnabled(true);
			}
		});
		((Button)findViewById(R.id.StopBtn)).setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
        		stopService(serviceIntent);
				serviceIntent = null;
				((Button)findViewById(R.id.StartBtn)).setEnabled(true);
				((Button)findViewById(R.id.StopBtn)).setEnabled(false);
			}
		});
	}
}