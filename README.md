# WebSocket联机下棋

#### 介绍
可在网页上联机玩象棋、五子棋、跳棋

服务器有.net控制台程序和安卓gradle工程
安卓gradle工程可以用vscode打开，无需AndroidStudio（需要设置好SDK和JDK路径）
服务器只做游戏房间的管理和消息的广播
server/debug中有编译好的exe和apk

客户端为html5网页，使用websocket连接服务器
游戏逻辑都在客户端，可扩展制作其他联机小游戏

手机上运行服务器后，可以用ipv6地址连接，就不限局域网联机哦