var com = com||{};

com.get = function (id){
	return document.getElementById(id)
}
window.onload = function(){
	let lastIp = localStorage.getItem("ip");
	if(!lastIp)
		lastIp = "localhost";
	com.get("ipInput").value = lastIp;
	com.get("connectServer").onclick=()=>{
		let ip = com.get("ipInput").value;
		net.connect(ip);
		localStorage.setItem("ip",ip);
	};
	com.get("createRoom").onclick=()=>{
		net.send(JSON.stringify({_:"createRoom", type:"dice", count:"2"}));
	};
	com.get("joinRoom").onclick=()=>{
		net.send(JSON.stringify({_:"getRoom", type:"dice"}));
	};
	com.get("quitRoom").onclick=()=>{
		if(net.isInRoom){
			net.isInRoom = false;
			net.send(JSON.stringify({_:"quitRoom"}));
			if(net.isHost)
				com.log("你解散了房间");
			else
				com.log("你离开了房间");
		}else{
			alert("你不在房间内");
		}
	};
	com.get("startGame").onclick=()=>{
		if(net.isInRoom){
			if(play.selfReady)
				alert("你已经准备过了");
			else
				net.send(JSON.stringify({_:"ready", user:net.userId}));
		}else{
			alert("你不在房间内");
		}
	};
	com.get("sendChat").onclick=()=>{
		if(net.isInRoom){
			net.send(JSON.stringify({_:"chat", user:net.userId, chat:com.get("chatInput").value}));
			com.get("chatInput").value = "";
		}else{
			alert("你不在房间内");
		}
	};
	com.get("createRoomCancel").onclick=()=>{
		net.isInRoom=false;
		com.get('createRoomDlg').close();
		net.send(JSON.stringify({_:'quitRoom'}));
		com.log("你解散了房间");
	};
}
com.log = function(msg){
	let div = document.createElement("div");
	div.innerHTML = msg;
	let info = com.get("info");
	if(info.childElementCount == 0)
		info.appendChild(div);
	else{
		if(info.childElementCount > 3)
			info.removeChild(info.lastChild);
		info.insertBefore(div, info.firstChild);
	}
}