var com = com||{};

com.init = function (){
	let scale = 1;
	if(window.innerWidth < 992)
		scale =	(window.innerWidth - 20) / 550;
	com.spaceX			=	60 * scale;
	com.spaceY			=	60 * scale;
	com.pointStartX		=	30 * scale;
	com.pointStartY		=	30 * scale;
	com.chessWidth		=	54 * scale;
	com.chessHeight		=	54 * scale;
	com.paneImg = new Image();
	com.paneImg.src  = "img/box.png";
	com.backImg = new Image();
	com.backImg.src = "img/back.png";
	com.imgs = new Array(2);
	let imgs = com.imgs;
	imgs[0] = new Array(7);
	imgs[1] = new Array(7);
	for(let i=0;i<7;++i){
		imgs[0][i]=new Image();
	}
	for(let i=0;i<7;++i){
		imgs[1][i]=new Image();
	}
	imgs[0][0].src = "img/r_j.png";
	imgs[0][1].src = "img/r_s.png";
	imgs[0][2].src = "img/r_x.png";
	imgs[0][3].src = "img/r_c.png";
	imgs[0][4].src = "img/r_m.png";
	imgs[0][5].src = "img/r_p.png";
	imgs[0][6].src = "img/r_z.png";
	imgs[1][0].src = "img/b_j.png";
	imgs[1][1].src = "img/b_s.png";
	imgs[1][2].src = "img/b_x.png";
	imgs[1][3].src = "img/b_c.png";
	imgs[1][4].src = "img/b_m.png";
	imgs[1][5].src = "img/b_p.png";
	imgs[1][6].src = "img/b_z.png";
	com.canvas = document.getElementById("canvas");
	com.canvas.width	=	550 * scale;
	com.canvas.height	=	300 * scale;
	com.ct = com.canvas.getContext("2d"); 
}
com.get = function (id){
	return document.getElementById(id)
}
window.onload = function(){
	let lastIp = localStorage.getItem("ip");
	if(!lastIp)
		lastIp = "localhost";
	com.get("ipInput").value = lastIp;
	com.get("connectServer").onclick=()=>{
		let ip = com.get("ipInput").value;
		net.connect(ip);
		localStorage.setItem("ip",ip);
	};
	com.get("createRoom").onclick=()=>{
		net.send(JSON.stringify({_:"createRoom", type:"jump", count:"2"}));
	};
	com.get("joinRoom").onclick=()=>{
		net.send(JSON.stringify({_:"getRoom", type:"jump"}));
	};
	com.get("quitRoom").onclick=()=>{
		if(net.isInRoom){
			net.isInRoom = false;
			net.send(JSON.stringify({_:"quitRoom"}));
			if(net.isHost)
				com.log("你解散了房间");
			else
				com.log("你离开了房间");
		}else{
			alert("你不在房间内");
		}
	};
	com.get("startGame").onclick=()=>{
		if(net.isInRoom){
			if(play.selfReady)
				alert("你已经准备过了");
			else
				net.send(JSON.stringify({_:"ready", user:net.userId}));
		}else{
			alert("你不在房间内");
		}
	};
	com.get("sendChat").onclick=()=>{
		if(net.isInRoom){
			net.send(JSON.stringify({_:"chat", user:net.userId, chat:com.get("chatInput").value}));
			com.get("chatInput").value = "";
		}else{
			alert("你不在房间内");
		}
	};
	com.get("retract").onclick=()=>{
		if(net.isInRoom){
			if(play.isPlay && play.my != play.turn && play.lastMove){
				net.send(JSON.stringify({_:"retract", user:net.userId}));
			}
		}else{
			alert("你不在房间内");
		}
	};
	com.get("createRoomCancel").onclick=()=>{
		net.isInRoom=false;
		com.get('createRoomDlg').close();
		net.send(JSON.stringify({_:'quitRoom'}));
		com.log("你解散了房间");
	};
}
com.getDomXY = function (dom){
	var left = dom.offsetLeft;
	var top = dom.offsetTop;
	var current = dom.offsetParent;
	while (current !== null){
		left += current.offsetLeft;
		top += current.offsetTop;
		current = current.offsetParent;
	}
	return {x:left,y:top};
}
com.log = function(msg){
	let div = document.createElement("div");
	div.innerHTML = msg;
	let info = com.get("info");
	if(info.childElementCount == 0)
		info.appendChild(div);
	else{
		if(info.childElementCount > 3)
			info.removeChild(info.lastChild);
		info.insertBefore(div, info.firstChild);
	}
}
com.init();