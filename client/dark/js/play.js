var play = play||{};

play.init = function (my){
	play.my=my;
	play.isPlay=false ;

	if(my==1){
		let chessList = [0,1,1,2,2,3,3,4,4,5,5,6,6,6,6,6,
			10,11,11,12,12,13,13,14,14,15,15,16,16,16,16,16
		];
		chessList.sort(function() {
			return Math.random() - 0.5;
		  });
		net.send(JSON.stringify({_:"initChess",chessList:chessList}));
	}

}

play.initChess = function(chessList){
	play.map = new Array(4);
	for(let i=0;i<4;i++){
		play.map[i] = new Array(8);
		for(let j=0;j<8;j++){
			let value_my=chessList[i*8+j];
			let value = value_my;
			if(value > 9)
				value -= 10;
			let my = value_my > 9 ? -1 : 1;
			let man = {value:value,my:my,show:false};
			play.map[i][j]=man;
		}
	}
	play.nowManKey=false;
	play.lastMove=false;
	play.isPlay=true ;
	play.selfReady=false;
	play.targetReady=false;
	play.show();
	play.turn = 1;
	com.log("对局开始，红方行动，你为" + (play.my==1?"红方":"黑方"));
}

play.show = function(){
	let ct=com.ct;
	ct.clearRect(0, 0, com.canvas.width, com.canvas.height);
	ct.globalAlpha=1;
	ct.beginPath();
	for(let m=0;m<5;m++){
		ct.moveTo(com.pointStartX, m*com.spaceY+com.pointStartY);
		ct.lineTo(8*com.spaceX+com.pointStartX, m*com.spaceY+com.pointStartY);
		ct.stroke();
	}
	for(let m=0;m<9;m++){
		ct.moveTo(m*com.spaceX+com.pointStartX, com.pointStartY);
		ct.lineTo(m*com.spaceX+com.pointStartX, 4*com.spaceY+com.pointStartY);
		ct.stroke();
	}
	ct.closePath();
	var img;
	for(let m=0;m<4;++m){
		for(let n=0;n<8;++n){
			let man = play.map[m][n];
			if(man){
				if(man.show){
					img = com.imgs[man.my==1 ? 0:1][man.value];
				}else{
					img = com.backImg;
				}
				if(play.nowManKey && play.nowManKey[0]==n && play.nowManKey[1]==m)
					ct.globalAlpha=0.6;
				else
					ct.globalAlpha=1;
				ct.drawImage(img,com.pointStartX+com.spaceX*n+(com.spaceX-com.chessWidth)/2,com.pointStartY+com.spaceY*m+(com.spaceY-com.chessHeight)/2,com.chessWidth,com.chessHeight);
			}
		}
	}
	ct.globalAlpha=1;
	if(play.lastMove){
		ct.drawImage(com.paneImg,com.pointStartX+com.spaceX*play.lastMove.x+(com.spaceX-com.chessWidth)/2,com.pointStartY+com.spaceY*play.lastMove.y+(com.spaceY-com.chessHeight)/2,com.chessWidth,com.chessHeight);
		if(play.lastMove.dx){
			ct.drawImage(com.paneImg,com.pointStartX+com.spaceX*play.lastMove.dx+(com.spaceX-com.chessWidth)/2,com.pointStartY+com.spaceY*play.lastMove.dy+(com.spaceY-com.chessHeight)/2,com.chessWidth,com.chessHeight);
		}
	}
}

play.checkMove = function(sx,sy,dx,dy){
	let man = play.map[sy][sx];
	if(!man || !man.show || man.my != play.turn || sx==dx&&sy==dy)
		return false;
	let eat = play.map[dy][dx];
	if(eat && (!eat.show||eat.my==man.my))
		return false;
	if(man.value==5 && eat){
		if(sx!=dx&&sy!=dy)
			return false;
		let n=0;
		if(sx==dx){
			if(sy<dy){
				for(let i=sy+1;i<dy;++i){
					if(play.map[i][sx])
						++n;
				}
			}else{
				for(let i=sy-1;i>dy;--i){
					if(play.map[i][sx])
						++n;
				}
			}
		}else{
			if(sx<dx){
				for(let i=sx+1;i<dx;++i){
					if(play.map[sy][i])
						++n;
				}
			}else{
				for(let i=sx-1;i>dx;--i){
					if(play.map[sy][i])
						++n;
				}
			}
		}
		if(n!=1)
			return false;
	}else if((Math.abs(sx-dx)!=1||sy!=dy) && (Math.abs(sy-dy)!=1||sx!=dx))
		return false;
	if(eat&&man.value!=5){
		if(man.value>eat.value && !(man.value==6&&eat.value==0) || (man.value==0&&eat.value==6))
			return false;
	}
	return true;
}

play.getClickPoint = function (e){
	var domXY = com.getDomXY(com.canvas);
	var x=Math.round((e.pageX-domXY.x-com.pointStartX-com.spaceX/2)/com.spaceX)
	var y=Math.round((e.pageY-domXY.y-com.pointStartY-com.spaceY/2)/com.spaceY)
	return {"x":x,"y":y}
}

com.canvas.addEventListener("click", function (e){
	if (!play.isPlay || play.my != play.turn) return false;
	let point = play.getClickPoint(e);
	let x = point.x;
	let y = point.y;
	if(x<0||x>7||y<0||y>3)
		return;
	let man = play.map[y][x];
	if (man){
		if (play.nowManKey && man.show && play.map[play.nowManKey[1]][play.nowManKey[0]].my != man.my){
			if(play.checkMove(play.nowManKey[0],play.nowManKey[1],x,y))
				net.send(JSON.stringify({_:"moveChess",sx:play.nowManKey[0],sy:play.nowManKey[1],dx:x,dy:y}));
		}else if(play.nowManKey[0] == x && play.nowManKey[1] == y){
			play.nowManKey = false;
			play.show();
			com.get("cancelAudio").play();
		}else if(!man.show){
			net.send(JSON.stringify({_:"showChess",x:x,y:y}));
		}else if (man.my===play.my){
			play.nowManKey = [x,y];
			play.show();
			com.get("selectAudio").play();
		}
		
	}else {
		if (play.nowManKey){
			if(play.checkMove(play.nowManKey[0],play.nowManKey[1],x,y))
				net.send(JSON.stringify({_:"moveChess",sx:play.nowManKey[0],sy:play.nowManKey[1],dx:x,dy:y}));
		}
	}
});

play.moveChess = function(sx,sy,dx,dy){
	let eat = play.map[dy][dx];
	let man = play.map[sy][sx];
	if(man.my!=play.turn)
		return;
	play.lastMove = {x:sx,y:sy,dx:dx,dy:dy,eat:eat};	//记录最后一步走棋信息用于悔棋
	play.map[dy][dx] = man;
	delete play.map[sy][sx];
	play.nowManKey = false;
	play.show();
	play.turn = -play.turn;
	if(eat){
		com.get("eatAudio").play();
		if(!play.checkWin())
			com.log(play.turn == 1 ? "红方行动" : "黑方行动");
	}else{
		com.get("clickAudio").play();
		com.log(play.turn == 1 ? "红方行动" : "黑方行动");
	}
}

play.showChess = function(x, y){
	let man=play.map[y][x];
	if(!man || man.show)
		return;
	man.show=true;
	play.nowManKey = false;
	play.lastMove = {x:x,y:y};
	play.show();
	play.turn = -play.turn;
	com.get("showAudio").play();
	com.log(play.turn == 1 ? "红方行动" : "黑方行动");
}


//悔棋
play.retract = function(){
	if(!play.lastMove || !play.lastMove.dx)
		return;
	let man = play.map[play.lastMove.dy][play.lastMove.dx];
	if(!man || man.my==play.turn)
		return;
	com.log(man.my==1?"红方悔棋":"黑方悔棋");
	play.map[play.lastMove.y][play.lastMove.x] = man;
	if(play.lastMove.eat)
		play.map[play.lastMove.dy][play.lastMove.dx]=play.lastMove.eat;
	else
		delete play.map[play.lastMove.dy][play.lastMove.dx];
	play.nowManKey = false;
	play.lastMove = {x:play.lastMove.x,y:play.lastMove.y};
	play.show();
	play.turn = -play.turn;
}

play.checkWin = function (){
	let redWin = true;
	let balackWin = true;
	for(let m=0;m<4;++m){
		for(let n=0;n<8;++n){
			let man = play.map[m][n];
			if(man){
				if(!man.show)
					return false;
				if(man.my==1)
					balackWin = false;
				else
					redWin = false;
			}
		}
	}
	if(redWin){
		play.showWin(play.my==1);
		return true;
	}
	else if(balackWin){
		play.showWin(play.my==-1);
		return true;
	}
	return false;
}
play.showWin = function (my){
	play.isPlay = false;
	if (my){
		com.log("恭喜你，你赢了！");
	}else{
		com.log("很遗憾，你输了！");
	}
}
