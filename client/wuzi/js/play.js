var play = play||{};

play.init = function (my){
	play.my = my;
	play.maps = new Array(15);
	var len = play.maps.length;
	for(let i=0;i<len;i++){
		play.maps[i] = new Array();
		for(let j = 0;j<len;j++){
			play.maps[i][j] = 0;
		}
	}
	play.show();
	play.lastPoint = false;
	play.isPlay  = true;
	play.selfReady = false;
	play.targetReady = false;
	play.turn = 1;
	com.log("对局开始，黑子先手，你为" + (net.isHost ? "黑子" : "白子"));
}
play.show = function(){
	com.ct.clearRect(0, 0, com.canvas.width, com.canvas.height);
	for(let m=0;m<15;m++){
		com.ct.fillText(String.fromCharCode(65+m),m*com.spaceX+com.pointStartX,com.canvas.height-10);
	}
	for(let n=0;n<15;n++){
		com.ct.fillText(15-n,7,n*com.spaceY+com.pointStartY);
	}
	for(let m=0;m<15;m++){
		for(let n=0;n<15;n++){
			if(m < 14 && n < 14)
				com.ct.strokeRect(m*com.spaceX+com.pointStartX,n*com.spaceY+com.pointStartY,com.spaceX,com.spaceY);
			if((m==3 || m==11) && (n==3 || n==11) || m==7 && n==7){
				com.ct.beginPath();
				com.ct.arc(m*com.spaceX+com.pointStartX,n*com.spaceY+com.pointStartY,4,0,2*Math.PI);
				com.ct.fill();
				com.ct.closePath();
			}
			if(play.maps[n][m] == 1){
				com.ct.drawImage(com.black,m*com.spaceX+com.pointStartX-com.chessWidth/2,n*com.spaceY,com.chessWidth,com.chessHeight);
			}else if(play.maps[n][m] == -1){
				com.ct.drawImage(com.white,m*com.spaceX+com.pointStartX-com.chessWidth/2,n*com.spaceY,com.chessWidth,com.chessHeight);
			}
		}
	}
}
com.canvas.addEventListener("click", function (e){
	if (!play.isPlay) return false;
	if(play.my != play.turn) return false;
	var point = play.getClickPoint(e);
	if (play.maps[point.y][point.x]===0){
		net.send(JSON.stringify({_:"clickPoint",x:point.x,y:point.y}));
	}
});
play.clickPoint = function (x,y){
	if(play.maps[y][x]===0){
		play.lastPoint={turn:play.turn,x:x,y:y};
		if(play.turn==1){
			play.maps[y][x] = 1;
			if(!play.iswin(1,y,x))
				com.log("黑" + String.fromCharCode(65+x) + (15-y));
			play.turn=-1;
		}else{
			play.maps[y][x] = -1;
			if(!play.iswin(-1,y,x))
				com.log("白" + String.fromCharCode(65+x) + (15-y));
			play.turn = 1;
		}
		play.show();
		com.ct.drawImage(com.paneImg,0,0,com.paneImg.width,com.paneImg.height,x*com.spaceX,y*com.spaceY,com.spaceX,com.spaceY);
		com.get("clickAudio").play();
	}
}
//悔棋
play.retract = function (){
	if(!play.lastPoint)
		return;
	if(play.lastPoint.turn==play.turn)
		return;
	com.log(play.lastPoint.turn==1?"黑方悔棋":"白方悔棋");
	play.maps[play.lastPoint.y][play.lastPoint.x] = 0;
	play.show();
	com.ct.drawImage(com.paneImg,0,0,com.paneImg.width,com.paneImg.height,play.lastPoint.x*com.spaceX,play.lastPoint.y*com.spaceY,com.spaceX,com.spaceY);
	play.turn = -play.turn;
}
play.iswin = function (t,orgy,orgx){
	var x = orgx;
	var y = orgy;
	var total = 1;
	while(x>0 &&play.maps[y][x-1]==t){
		total++;
		x--;
		
	};
	x = orgx;
	y = orgy;
	while(x<14 &&play.maps[y][x+1]==t){
		x++;
		total++;
	};
	if(total>4){
		play.showWin(t);
		return true;
	}
	
	x = orgx;
	y = orgy;
	total = 1;
	while(y>0&&play.maps[y-1][x]==t){
		total++;
		y--;
	}
	x = orgx;
	y = orgy;
	while(y<14&&play.maps[y+1][x]==t){
		total++;
		y++;
	}
	if(total>4){
		play.showWin(t);
		return true;
	}

	x = orgx;
	y = orgy;
	total = 1;
	while(y>0&&x>0&&play.maps[y-1][x-1]==t){
		y--;
		x--;
		total++;
	}
	x = orgx;
	y = orgy;
	while(y<14&&x<14&&play.maps[y+1][x+1]==t){
		y++;
		x++;
		total++;
	}
	if(total>4){
		play.showWin(t);
		return true;
	}

	x = orgx;
	y = orgy;
	total = 1;
	while(y>0&&x<14&&play.maps[y-1][x+1]==t){
		y--;
		x++;
		total++;
	}
	x = orgx;
	y = orgy;
	while(y<14&&x>0&&play.maps[y+1][x-1]==t){
		y++;
		x--;
		total++;
	}
	if(total>4){
		play.showWin(t);
		return true;
	}
}
play.getClickPoint = function (e){
	var domXY = com.getDomXY(com.canvas);
	var x=Math.round((e.pageX-domXY.x-com.pointStartX)/(com.spaceX))
	var y=Math.round((e.pageY-domXY.y-com.pointStartY)/(com.spaceY))
	return {"x":x,"y":y}
}
play.showWin = function (my){
	play.isPlay = false;
	if (my===play.my){
		com.log("恭喜你，你赢了！");
	}else{
		com.log("很遗憾，你输了！");
	}
}
