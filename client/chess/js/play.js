var play = play||{};

play.init = function (my){
	play.my				=	my;				//玩家方
	if(my==1)
		play.map 			=	com.arr2Clone (com.initMapRed);		//初始化棋盘
	else
		play.map 			=	com.arr2Clone (com.initMapBlack);
	play.nowManKey		=	false;			//现在要操作的棋子
	play.lastMove		= 	false;
	play.isPlay 		=	true ;			//是否能走棋
	play.bylaw 			= 	com.bylaw;
	play.show 			= 	com.show;
	play.showPane 		= 	com.showPane;
	com.pane.isShow		=	 false;			//隐藏方块
	play.selfReady = false;
	play.targetReady = false;
	
	//初始化棋子
	for (var i=0; i<play.map.length; i++){
		for (var n=0; n<play.map[i].length; n++){
			var key = play.map[i][n];
			if (key){
				com.mans[key].x=n;
				com.mans[key].y=i;
				com.mans[key].isShow = true;
			}
		}
	}
	play.show();
	play.turn = 1;
	com.log("对局开始，红方行动");
}

//点击棋盘事件
com.canvas.addEventListener("click", function (e){
	if (!play.isPlay || play.my != play.turn || com.moveKey) return false;
	var key = play.getClickMan(e);
	var point = play.getClickPoint(e);
	var x = point.x;
	var y = point.y;
	
	if (key){
		let man = com.mans[key];
		if (play.nowManKey && man.my != com.mans[play.nowManKey ].my){
			if (play.indexOfPs(com.mans[play.nowManKey].ps,[x,y])){
				net.send(JSON.stringify({_:"moveChess",key:play.nowManKey,x:x,y:y}));
			}
		}else if(play.nowManKey == key){
			man.alpha = 1;
			play.nowManKey = false;
			com.dot.dots = [];
			com.show();
			com.get("cancelAudio").play();
		}else{
			if (man.my===play.turn){
				if (com.mans[play.nowManKey])
					com.mans[play.nowManKey].alpha = 1 ;
				man.alpha = 0.6;
				play.nowManKey = key;
				com.mans[key].ps = com.mans[key].bl();
				com.dot.dots = com.mans[key].ps;
				com.show();
				com.get("selectAudio").play();
			}
		}
	}else {
		if (play.nowManKey){
			if (play.indexOfPs(com.mans[play.nowManKey].ps,[x,y])){
				net.send(JSON.stringify({_:"moveChess",key:play.nowManKey,x:x,y:y}));
			}
		}
	}
});

play.moveChess = function(key, x, y){
	if(play.my!=play.turn){
		x=8-x;
		y=9-y;
	}
	let eat = play.map[y][x];
	let man = com.mans[key];
	if(man.my!=play.turn)
		return;
	com.log(com.createMove(man,x,y,eat));
	play.lastMove = {man:man,x:man.x,y:man.y,eat:eat};	//记录最后一步走棋信息用于悔棋
	delete play.map[man.y][man.x];
	play.map[y][x] = key;
	com.showPane(man.x ,man.y,x,y)
	man.alpha = 1;
	play.nowManKey = false;
	com.dot.dots = [];
	com.moveKey = key;
	com.moveX = x;
	com.moveY = y;
	play.turn = -play.turn;
	let move = setInterval(() => {
		if(com.moveKey)
			com.show();
		else{
			clearInterval(move);
			if(eat){
				com.mans[eat].isShow = false;
				com.show();
				com.get("eatAudio").play();
				if (eat == "j0") play.showWin(-1);
				if (eat == "J0") play.showWin(1);
			}else{
				com.get("clickAudio").play();
			}
			play.checkJiang();
		}
	}, 60);
}

//检查将军
play.checkJiang = function(){
	var jx,jy;
	if(play.turn === 1) {
		jx = com.mans['j0'].x;
		jy = com.mans['j0'].y;
	}else{
		jx = com.mans['J0'].x;
		jy = com.mans['J0'].y;
	}
	for(let key in com.mans) {
		let man = com.mans[key];
		if(man.isShow && man.my !== play.turn)
		{
			let ps = man.bl();
			for (let i=0; i<ps.length; i++){
				if (ps[i][0]==jx&&ps[i][1]==jy){
					com.get("jiangAudio").play();
					return;
				}
			}
		}
	}
}

//悔棋
play.retract = function(){
	if(!play.lastMove)
		return;
	if(play.lastMove.man.my==play.turn)
		return;
	com.log(play.lastMove.man.my==1?"红方悔棋":"黑方悔棋");
	let nowX = play.lastMove.man.x;
	let nowY = play.lastMove.man.y;
	let lastX = play.lastMove.x;
	let lastY = play.lastMove.y;
	play.lastMove.man.x = lastX;
	play.lastMove.man.y = lastY;
	play.map[lastY][lastX] = play.lastMove.man.key;
	if(play.lastMove.eat){
		play.map[nowY][nowX]=play.lastMove.eat;
		com.mans[play.lastMove.eat].isShow = true;
	}
	else
		delete play.map[nowY][nowX];
	
	com.showPane(nowX,nowY,lastX,lastY);
	if(play.nowManKey){
		com.mans[play.nowManKey].alpha = 1;
		play.nowManKey = false;
	}
	com.dot.dots = [];
	play.lastMove = false;
	com.show();
	play.turn = -play.turn;
}

play.indexOfPs = function (ps,xy){
	for (var i=0; i<ps.length; i++){
		if (ps[i][0]==xy[0]&&ps[i][1]==xy[1]) return true;
	}
	return false;
	
}

//获得点击的着点
play.getClickPoint = function (e){
	var domXY = com.getDomXY(com.canvas);
	var x=Math.round((e.pageX-domXY.x-com.pointStartX)/com.spaceX)
	var y=Math.round((e.pageY-domXY.y-com.pointStartY)/com.spaceY)
	return {"x":x,"y":y}
}

//获得棋子
play.getClickMan = function (e){
	var clickXY=play.getClickPoint(e);
	var x=clickXY.x;
	var y=clickXY.y;
	if (x < 0 || x>8 || y < 0 || y > 9) return false;
	return play.map[y][x];
}

play.showWin = function (my){
	play.isPlay = false;
	if (my===play.my){
		com.log("恭喜你，你赢了！");
	}else{
		com.log("很遗憾，你输了！");
	}
}

