var play = play||{};

play.rowCount = 16;
play.cellCount = 16;
play.mineCount = 50;
play.tb = com.get("grids");
for(let i=0;i<play.rowCount;++i){
	let tr = document.createElement("tr");
	play.tb.appendChild(tr);
	for(let j=0;j<play.cellCount;++j){
		let td = document.createElement("td");
		tr.appendChild(td);
	}
}

play.init = function (my){
	play.my=my;
	play.isPlay=false ;

	if(my==1){
		let grids = new Array(play.rowCount * play.cellCount);
		for(let i=0;i<grids.length;++i){
			grids[i]=i;
		}
		let mines = [];
		while (mines.length < play.mineCount) {
			let index = Math.floor(Math.random() * grids.length);
			mines.push(grids[index]);
			grids.splice(index, 1);
		}
		let turn=1;
		if(Math.random() < 0.5)
			turn=-1;
		net.send(JSON.stringify({_:"initMine",mines:mines,turn:turn}));
	}

}

play.initMine = function(mines,turn){
	play.mines = mines;
	play.isPlay=true ;
	play.selfReady=false;
	play.targetReady=false;
	play.turn = turn;
	play.clickCount = play.rowCount * play.cellCount - play.mineCount;
	for(let i=0;i<play.rowCount;++i){
		for(let j=0;j<play.cellCount;++j){
			play.tb.rows[i].cells[j].innerHTML="";
		}
	}
	if(play.lastIdx){
		play.tb.rows[Math.floor(play.lastIdx/play.cellCount)].cells[play.lastIdx%play.cellCount].className="tdnormal";
		play.lastIdx = false;
	}
	com.log("对局开始，" + (play.my==play.turn?"你先扫雷":"对方扫雷"));
}

play.tb.addEventListener("click", function(event) {
	let target = event.target;
	if (target.tagName.toLowerCase() === "td") {
		if(play.isPlay && play.turn == play.my && target.innerHTML == "")
			net.send(JSON.stringify({_:"click", idx:target.parentNode.rowIndex*play.cellCount+target.cellIndex}));
	}
});

play.click = function(idx){
	if(play.lastIdx)
		play.tb.rows[Math.floor(play.lastIdx/play.cellCount)].cells[play.lastIdx%play.cellCount].className="tdnormal";
	play.lastIdx = idx;
	play.tb.rows[Math.floor(idx/play.cellCount)].cells[idx%play.cellCount].className="tdlast";
	if(play.mines.includes(idx)){
		for(let i = 0;i<play.mines.length;++i){
			play.tb.rows[Math.floor(play.mines[i]/play.cellCount)].cells[play.mines[i]%play.cellCount].innerHTML="*";
		}
		com.get("bomb").play();
		play.showWin(play.turn != play.my);
	}else{
		play.showMineCount(idx);
		if(play.clickCount<=0){
			com.log("所有雷已扫出");
			play.showWin(true);
		}
		else{
			play.turn = -play.turn;
			com.log(play.turn == play.my ? "该你扫雷" : "对方扫雷");
		}
	}
}

play.showMineCount = function(idx){
	let td = play.tb.rows[Math.floor(idx/play.cellCount)].cells[idx%play.cellCount];
	if(td.innerHTML != "") return;
	let mineCount = play.getMineCount(idx);
	td.innerHTML=mineCount;
	--play.clickCount;
	if(mineCount == 0){
		let row = Math.floor(idx/play.cellCount);
		let cell = idx%play.cellCount;
		if(row > 0){
			play.showMineCount(idx - play.cellCount);
			if(cell > 0) play.showMineCount(idx - play.cellCount - 1);
			if(cell < play.cellCount - 1) play.showMineCount(idx - play.cellCount + 1);
		}
		if(row < play.rowCount - 1){
			play.showMineCount(idx + play.cellCount);
			if(cell > 0) play.showMineCount(idx + play.cellCount - 1);
			if(cell < play.cellCount - 1) play.showMineCount(idx + play.cellCount + 1);
		}
		if(cell > 0) play.showMineCount(idx - 1);
		if(cell < play.cellCount - 1) play.showMineCount(idx + 1);
	}
}

play.getMineCount = function(idx){
	let count = 0;
	let row = Math.floor(idx/play.cellCount);
	let cell = idx%play.cellCount;
	if(row > 0){
		if(play.mines.includes(idx - play.cellCount)) ++count;
		if(cell > 0 && play.mines.includes(idx - play.cellCount - 1)) ++count;
		if(cell < play.cellCount - 1 && play.mines.includes(idx - play.cellCount + 1)) ++count;
	}
	if(row < play.rowCount - 1){
		if(play.mines.includes(idx + play.cellCount)) ++count;
		if(cell > 0 && play.mines.includes(idx + play.cellCount - 1)) ++count;
		if(cell < play.cellCount - 1 && play.mines.includes(idx + play.cellCount + 1)) ++count;
	}
	if(cell > 0 && play.mines.includes(idx - 1)) ++count;
	if(cell < play.cellCount - 1 && play.mines.includes(idx + 1)) ++count;
	return count;
}

play.showWin = function (my){
	play.isPlay = false;
	if (my){
		com.log("恭喜你，你赢了！");
	}else{
		com.log("很遗憾，你输了！");
	}
}
