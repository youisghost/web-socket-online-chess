var play = play||{};

play.init = function (my){
	play.my = my;
	play.maps = new Array(17);
	let lenY = play.maps.length;
	for(let i=0;i<lenY;i++){
		let lenX = 9-Math.abs(i-8);
		play.maps[i] = new Array(lenX);
		for(let j = 0;j<lenX;j++){
			if(i < 4)
				play.maps[i][j] = -my;
			else if(i > 12)
				play.maps[i][j] = my;
			else
				play.maps[i][j] = 0;
		}
	}
	play.ps = new Map();
	play.nowManKey = false;
	play.lastMove = false;
	play.show();
	play.isPlay  = true;
	play.selfReady = false;
	play.targetReady = false;
	play.turn = 1;
	com.log("对局开始，红方先手");
}
play.show = function(){
	let ct=com.ct;
	ct.clearRect(0, 0, com.canvas.width, com.canvas.height);
	ct.globalAlpha=1;
	ct.beginPath();
	ct.moveTo(com.canvas.width / 2, com.pointStartY);
	ct.lineTo((com.canvas.width - com.spaceX * 3) / 2, 3*com.spaceY+com.pointStartY);
	ct.lineTo((com.canvas.width + com.spaceX * 3) / 2, 3*com.spaceY+com.pointStartY);
	ct.fillStyle=play.my==1?"blue":"red";
	ct.fill();
	ct.closePath();
	ct.beginPath();
	ct.moveTo(com.canvas.width / 2, 16*com.spaceY+com.pointStartY);
	ct.lineTo((com.canvas.width - com.spaceX * 3) / 2, 13*com.spaceY+com.pointStartY);
	ct.lineTo((com.canvas.width + com.spaceX * 3) / 2, 13*com.spaceY+com.pointStartY);
	ct.fillStyle=play.my==1?"red":"blue";
	ct.fill();
	ct.closePath();
	ct.beginPath();
	for(let m=1;m<16;m++){
		ct.moveTo((com.canvas.width - com.spaceX * (8-Math.abs(m-8))) / 2, m*com.spaceY+com.pointStartY);
		ct.lineTo((com.canvas.width + com.spaceX * (8-Math.abs(m-8))) / 2, m*com.spaceY+com.pointStartY);
		ct.stroke();
	}
	for(let m=0;m<9;m++){
		ct.moveTo((com.canvas.width - com.spaceX * m) / 2, m*com.spaceY+com.pointStartY);
		ct.lineTo((com.canvas.width + com.spaceX * (8-m)) / 2, (m + 8)*com.spaceY+com.pointStartY);
		ct.stroke();
	}
	for(let m=0;m<9;m++){
		ct.moveTo((com.canvas.width + com.spaceX * m) / 2, m*com.spaceY+com.pointStartY);
		ct.lineTo((com.canvas.width - com.spaceX * (8-m)) / 2, (m + 8)*com.spaceY+com.pointStartY);
		ct.stroke();
	}
	ct.closePath();
	ct.fillStyle="black";
	for(let m=0;m<17;m++){
		for(let n=0;n<=8-Math.abs(m-8);n++){
			ct.beginPath();
			ct.arc((com.canvas.width - com.spaceX * (8-Math.abs(m-8) - 2*n)) / 2,m*com.spaceY+com.pointStartY,4,0,2*Math.PI);
			ct.fill();
			ct.closePath();
		}
	}
	for(let m=0;m<play.maps.length;m++){
		for(let n=0;n<play.maps[m].length;n++){
			if(play.nowManKey&&play.nowManKey[0]==n&&play.nowManKey[1]==m)
				ct.globalAlpha=0.6;
			else
				ct.globalAlpha=1;
			if(play.maps[m][n] == 1){
				ct.drawImage(com.red,(com.canvas.width - com.spaceX * (8-Math.abs(m-8) - 2*n) - com.chessWidth) / 2,m*com.spaceY+com.pointStartY - com.chessHeight/2,com.chessWidth,com.chessHeight);
			}else if(play.maps[m][n] == -1){
				ct.drawImage(com.blue,(com.canvas.width - com.spaceX * (8-Math.abs(m-8) - 2*n) - com.chessWidth) / 2,m*com.spaceY+com.pointStartY - com.chessHeight/2,com.chessWidth,com.chessHeight);
			}
		}
	}
	ct.globalAlpha=1;
	if(play.lastMove){
		let dest = play.lastMove.ps[play.lastMove.ps.length-1];
		ct.drawImage(com.paneImg,(com.canvas.width - com.spaceX * (8-Math.abs(play.lastMove.y-8) - 2*play.lastMove.x) - com.chessWidth) / 2,play.lastMove.y*com.spaceY+com.pointStartY - com.chessHeight/2,com.chessWidth,com.chessHeight);
		ct.drawImage(com.paneImg,(com.canvas.width - com.spaceX * (8-Math.abs(dest[1]-8) - 2*dest[0]) - com.chessWidth) / 2,dest[1]*com.spaceY+com.pointStartY - com.chessHeight/2,com.chessWidth,com.chessHeight);
		for(let m=0;m<play.lastMove.ps.length-1;++m){
			let point=play.lastMove.ps[m];
			ct.drawImage(com.dot,(com.canvas.width - com.spaceX * (8-Math.abs(point[1]-8) - 2*point[0]) - com.dotWidth) / 2,point[1]*com.spaceY+com.pointStartY - com.dotHeight/2,com.dotWidth,com.dotHeight);
		}
	}
	// if(play.nowManKey){
	// 	for (const [key,ps] of play.ps) {
	// 		let point=ps[ps.length-1];
	// 		ct.drawImage(com.dot,(com.canvas.width - com.spaceX * (8-Math.abs(point[1]-8) - 2*point[0]) - com.dotWidth) / 2,point[1]*com.spaceY+com.pointStartY - com.dotHeight/2,com.dotWidth,com.dotHeight);
	// 	}
	// }
}
com.canvas.addEventListener("click", function (e){
	if (!play.isPlay) return false;
	if(play.my != play.turn) return false;
	var point = play.getClickPoint(e);
	if(point){
		if (play.maps[point.y][point.x]===play.my){
			if(play.nowManKey){
				play.nowManKey = false;
				com.get("cancelAudio").play();
			}
			else{
				play.nowManKey=[point.x,point.y];
				play.ps = play.getPs();
				com.get("selectAudio").play();
			}
			play.show();
		}else if (play.maps[point.y][point.x]===0){
			if(play.nowManKey){
				let key=point.x+"_"+point.y;
				if(play.ps.has(key)){
					net.send(JSON.stringify({_:"moveChess",x:play.nowManKey[0],y:play.nowManKey[1],ps:play.ps.get(key)}));
				}
			}
		}
	}
});
play.getClickPoint = function (e){
	var domXY = com.getDomXY(com.canvas);
	var dy = e.pageY-domXY.y-com.pointStartY;
	var y=Math.round(dy/com.spaceY);
	if(y < 0 || y >= play.maps.length)
		return false;
	if(dy < y*com.spaceY-com.chessHeight/2 || dy > y*com.spaceY+com.chessHeight/2)
		return false;
	var dx = e.pageX-domXY.x - (com.canvas.width - (play.maps[y].length-1)*com.spaceX)/2;
	var x=Math.round(dx/com.spaceX);
	if(x < 0 || x >= play.maps[y].length)
		return false;
	if(dx < x*com.spaceX-com.chessWidth/2 || dx > x*com.spaceX+com.chessWidth/2)
		return false;
	return {"x":x,"y":y}
}

play.moveChess = function(x, y, ps){
	if(play.my!=play.turn){
		y=16-y;
		x=play.maps[y].length-1-x;
		ps.forEach(element => {
			element[1]=16-element[1];
			element[0]=play.maps[element[1]].length-1-element[0];
		});
	}
	if(play.maps[y][x]!=play.turn)
		return;
	play.lastMove = {turn:play.turn,x:x,y:y,ps:ps};
	play.nowManKey = false;
	play.maps[ps[0][1]][ps[0][0]] = play.maps[y][x];
	play.maps[y][x] = 0;
	play.show();
	com.get("clickAudio").play();
	play.turn = -play.turn;
	if(ps.length > 1){
		let idx = 0;
		play.isPlay = false;
		let move = setInterval(() => {
			let now = ps[idx];
			let dest = ps[++idx];
			play.maps[dest[1]][dest[0]] = play.maps[now[1]][now[0]];
			play.maps[now[1]][now[0]] = 0;
			play.show();
			com.get("clickAudio").play();
			if(idx >= ps.length - 1){
				clearInterval(move);
				play.checkWin();
			}
		}, 500);
	}
	else{
		play.checkWin();
	}
}
play.getLeft = function(x,y){
	if(x>0)
		return[x-1,y];
	return false;
}
play.getRight = function(x,y){
	if(x<play.maps[y].length-1)
		return[x+1,y];
	return false;
}
play.getLeftUp = function(x,y){
	if(y<9){
		if(x>0)
			return [x-1,y-1];
	}else{
		return [x,y-1];
	}
	return false;
}
play.getRightUp = function(x,y){
	if(y<9){
		if(x<play.maps[y].length-1)
			return [x,y-1];
	}else{
		return[x+1,y-1];
	}
	return false;
}
play.getLeftDown = function(x,y){
	if(y<8){
		return [x,y+1];
	}else{
		if(x>0)
			return [x-1,y+1];
	}
	return false;
}
play.getRightDown = function(x,y){
	if(y<8){
		return [x+1,y+1];
	}else{
		if(x<play.maps[y].length-1)
			return [x,y+1];
	}
	return false;
}
//可以走棋的点
play.getPs = function(){
	let x=play.nowManKey[0];
	let y=play.nowManKey[1];
	let ps=play.ps;
	ps.clear();
	let maps=play.maps;

	play.getJump(x,y,[]);

	let left = play.getLeft(x,y);
	if(left && maps[left[1]][left[0]]==0)
		ps.set(left[0]+"_"+left[1],[left]);
	
	let right = play.getRight(x,y);
	if(right && maps[right[1]][right[0]]==0)
		ps.set(right[0]+"_"+right[1],[right]);

	let leftUp = play.getLeftUp(x,y);
	if(leftUp && maps[leftUp[1]][leftUp[0]]==0)
		ps.set(leftUp[0]+"_"+leftUp[1],[leftUp]);
	
	let rightUp = play.getRightUp(x,y);
	if(rightUp && maps[rightUp[1]][rightUp[0]]==0)
		ps.set(rightUp[0]+"_"+rightUp[1],[rightUp]);
	
	let leftDown = play.getLeftDown(x,y);
	if(leftDown && maps[leftDown[1]][leftDown[0]]==0)
		ps.set(leftDown[0]+"_"+leftDown[1],[leftDown]);
		
	let rightDown = play.getRightDown(x,y);
	if(rightDown && maps[rightDown[1]][rightDown[0]]==0)
		ps.set(rightDown[0]+"_"+rightDown[1],[rightDown]);

	return ps;
}
play.clonePs = function(point, parent){
	let data = [];
	parent.forEach(element => {
		data.push(element);
	});
	data.push(point);
	return data;
}
play.checkAddPs = function(point,parent){
	let ps = play.ps;
	let key = point[0] + "_" + point[1];
	if(!ps.has(key)||ps.get(key).length > parent.length + 1){
		let value=play.clonePs(point,parent);
		ps.set(key,value);
		play.getJump(point[0],point[1],value);
	}
}
//获取可以跳的点
play.getJump = function(x,y,parent){
	let maps=play.maps;

	let left = play.getLeft(x,y);
	if(left && maps[left[1]][left[0]]!=0){
		let left2 = play.getLeft(left[0],left[1]);
		if(left2 && maps[left2[1]][left2[0]]==0){
			play.checkAddPs(left2,parent);
		}
	}
	
	let right = play.getRight(x,y);
	if(right && maps[right[1]][right[0]]!=0){
		let right2 = play.getRight(right[0],right[1]);
		if(right2 && maps[right2[1]][right2[0]]==0){
			play.checkAddPs(right2,parent);
		}
	}

	let leftUp = play.getLeftUp(x,y);
	if(leftUp && maps[leftUp[1]][leftUp[0]]!=0){
		let leftUp2 = play.getLeftUp(leftUp[0],leftUp[1]);
		if(leftUp2 && maps[leftUp2[1]][leftUp2[0]]==0){
			play.checkAddPs(leftUp2,parent);
		}
	}
	
	let rightUp = play.getRightUp(x,y);
	if(rightUp && maps[rightUp[1]][rightUp[0]]!=0){
		let rightUp2 = play.getRightUp(rightUp[0],rightUp[1]);
		if(rightUp2 && maps[rightUp2[1]][rightUp2[0]]==0){
			play.checkAddPs(rightUp2,parent);
		}
	}
	
	let leftDown = play.getLeftDown(x,y);
	if(leftDown && maps[leftDown[1]][leftDown[0]]!=0){
		let leftDown2 = play.getLeftDown(leftDown[0],leftDown[1]);
		if(leftDown2 && maps[leftDown2[1]][leftDown2[0]]==0){
			play.checkAddPs(leftDown2,parent);
		}
	}
		
	let rightDown = play.getRightDown(x,y);
	if(rightDown && maps[rightDown[1]][rightDown[0]]!=0){
		let rightDown2 = play.getRightDown(rightDown[0],rightDown[1]);
		if(rightDown2 && maps[rightDown2[1]][rightDown2[0]]==0){
			play.checkAddPs(rightDown2,parent);
		}
	}
}
//悔棋
play.retract = function (){
	if(!play.lastMove)
		return;
	if(play.lastMove.turn==play.turn)
		return;
	com.log(play.lastMove.turn==1?"红方悔棋":"蓝方悔棋");
	let dest = play.lastMove.ps[play.lastMove.ps.length-1];
	play.maps[play.lastMove.y][play.lastMove.x] = play.maps[dest[1]][dest[0]];
	play.maps[dest[1]][dest[0]] = 0;
	play.show();
	play.turn = -play.turn;
}
play.checkWin = function (){
	let selfWin = true;
	for(let m=0;m<4;++m){
		for(let n=0;n<play.maps[m].length;++n){
			if(play.maps[m][n] != play.my){
				selfWin = false;
				break;
			}
		}
	}
	if(selfWin){
		play.showWin(true);
		return;
	}
	let targetWin = true;
	for(let m=13;m<17;++m){
		for(let n=0;n<play.maps[m].length;++n){
			if(play.maps[m][n] != -play.my){
				targetWin = false;
				break;
			}
		}
	}
	if(targetWin){
		play.showWin(false);
		return;
	}
	play.isPlay = true;
	com.log(play.turn == 1 ? "红方行动" : "蓝方行动");
}
play.showWin = function (my){
	play.isPlay = false;
	if (my){
		com.log("恭喜你，你赢了！");
	}else{
		com.log("很遗憾，你输了！");
	}
}